﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'
// Upgrade NOTE: replaced '_ProjectorClip' with 'unity_ProjectorClip'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/textureProject"
{
    Properties
    {		
		_MainTex("Projected Image", 2D) = "white" {}
		_ShadowTex("Background Image", 2D) = "white" {}
	}
	SubShader
	{
		Pass
		{
			ZWrite Off
			Blend Off
			//Offset - 1, -1

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			uniform sampler2D _MainTex;

			uniform float4x4 unity_Projector;

			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct vertexOutput {
				float4 pos: SV_POSITION;
				float4 posProj: TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.posProj = mul(unity_Projector, input.vertex);
				output.pos = UnityObjectToClipPos(input.vertex);

				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				if (input.posProj.w > 0.0) // in front of projector?
				{
					return tex2D(_MainTex,
						input.posProj.xy / input.posProj.w);
				}
				else // behind projector
				{
					return float4(0.0, 0.0, 0.0, 0.0);
				}
			}

			ENDCG
		}

		Pass
		{
			ZWrite Off
			//Blend Off
			Blend OneMinusDstColor One
			//Offset - 1, -1

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
		
			uniform sampler2D _ShadowTex;

			uniform float4x4 unity_Projector;

			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct vertexOutput {
				float4 pos: SV_POSITION;
				float4 posProj: TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.posProj = mul(unity_Projector, input.vertex);
				output.pos = UnityObjectToClipPos(input.vertex);

				return output;
			}

			float4 frag(vertexOutput input): COLOR
			{
				if (input.posProj.w > 0.0) // in front of projector?
				{
					return tex2D(_ShadowTex,
						input.posProj.xy / input.posProj.w);
				}
				else // behind projector
				{
					return float4(0.0, 0.0, 0.0, 0.0);
				}
			}

			ENDCG
		}
	}

	FallBack "Projector/Light"
	
}
