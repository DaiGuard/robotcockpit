﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class headAudioCtrl : MonoBehaviour
{
    private Animator animator;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AudioStart()
    {
        animator.SetBool("EXIT", false);
        audioSource.PlayOneShot(audioSource.clip);
    }

    public void Restart()
    {
        animator.SetBool("EXIT", true);
    }
}
