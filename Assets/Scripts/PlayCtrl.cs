﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayCtrl : MonoBehaviour
{
    private Animator animator;

    private bool playStatus = false;

    public bool PlayStatus
    {
        get { return this.playStatus;  }
    }

    private Dropdown mainDrop;
    private Dropdown rightDrop;
    private Dropdown leftDrop;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        mainDrop = GameObject.Find("mainDropdown").GetComponent<Dropdown>();
        rightDrop = GameObject.Find("rightDropdown").GetComponent<Dropdown>();
        leftDrop = GameObject.Find("leftDropdown").GetComponent<Dropdown>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        var flag = animator.GetBool("START");

        playStatus = !flag;

        animator.SetBool("START", playStatus);

        if (playStatus)
        {
            mainDrop.interactable = false;
            rightDrop.interactable = false;
            leftDrop.interactable = false;
        }
        else
        {
            mainDrop.interactable = true;
            rightDrop.interactable = true;
            leftDrop.interactable = true;
        }
    }
}
