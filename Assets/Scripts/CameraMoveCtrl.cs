﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraMoveCtrl : MonoBehaviour
{
    private Animator animator;

    private GameObject upButton;
    private GameObject downButton;
    private GameObject rightButton;
    private GameObject leftButton;

    private Camera cam;

    private AudioSource[] audioSource;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        upButton = GameObject.Find("UpButton");
        downButton = GameObject.Find("DownButton");
        rightButton = GameObject.Find("RightButton");
        leftButton = GameObject.Find("LeftButton");

        cam = GetComponent<Camera>();

        audioSource = GetComponents<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {        
        if (!audioSource[0].isPlaying)
        {
            if(!audioSource[1].isPlaying)
                audioSource[1].Play();
        }
        else
        {
            if (audioSource[1].isPlaying)
                audioSource[1].Stop();
        }
    }

    public void UpButtonClick()
    {
        animator.SetBool("UP", true);
    }

    public void DownButtonClick()
    {
        animator.SetBool("DOWN", true);
    }

    public void RightButtonClick()
    {
        animator.SetBool("RIGHT", true);
    }

    public void LeftButtonClick()
    {
        animator.SetBool("LEFT", true);
    }

    public void MainCameraPosMoved()
    {
        animator.SetBool("RIGHT", false);
        animator.SetBool("LEFT", false);
        animator.SetBool("DOWN", false);

        downButton.SetActive(false);

        upButton.SetActive(true);
        rightButton.SetActive(true);
        leftButton.SetActive(true);
    }

    public void UICameraPosMoved()
    {
        animator.SetBool("UP", false);

        upButton.SetActive(false);

        downButton.SetActive(true);
        rightButton.SetActive(true);
        leftButton.SetActive(true);
    }

    public void RightCameraPosMoved()
    {
        animator.SetBool("RIGHT", false);

        rightButton.SetActive(false);
        downButton.SetActive(false);
        
        upButton.SetActive(true);
        leftButton.SetActive(true);
    }

    public void LeftCameraPosMoved()
    {
        animator.SetBool("LEFT", false);

        leftButton.SetActive(false);
        downButton.SetActive(false);

        upButton.SetActive(true);
        rightButton.SetActive(true);
    }

    public void Restart()
    {
        animator.SetBool("EXIT", true);
    }

    public void AudioStart()
    {
        animator.SetBool("EXIT", false);

        audioSource[0].PlayOneShot(audioSource[0].clip);
    }
}
