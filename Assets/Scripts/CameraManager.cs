﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CameraManager : MonoBehaviour
{
    
    private Dropdown main;
    private Dropdown right;
    private Dropdown left;

    // Start is called before the first frame update
    void Start()
    {
        main = GameObject.Find("mainDropdown").GetComponent<Dropdown>();
        right = GameObject.Find("rightDropdown").GetComponent<Dropdown>();
        left = GameObject.Find("leftDropdown").GetComponent<Dropdown>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateDeviceList()
    {
        var devices = WebCamTexture.devices;
        var list = new List<Dropdown.OptionData>();

        var num = 1;
        foreach (WebCamDevice dev in devices)
        {
            list.Add(new Dropdown.OptionData(num.ToString() + ": " + dev.name));
            num++;
        }

        main.options.RemoveRange(1, main.options.Count - 1);
        right.options.RemoveRange(1, right.options.Count - 1);
        left.options.RemoveRange(1, left.options.Count - 1);        

        foreach (var data in list)
        {
            main.options.Add(data);
            right.options.Add(data);
            left.options.Add(data);
        }
    }
}
