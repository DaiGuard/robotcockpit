﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshCtrl : MonoBehaviour
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        animator.SetBool("REFRESH", true);
        animator.SetInteger("COUNT", 0);
    }

    public void EndRefresh()
    {        
        animator.SetBool("REFRESH", false);

        var num = animator.GetInteger("COUNT");
        animator.SetInteger("COUNT", num+1);
    }
}
