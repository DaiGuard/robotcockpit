﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectAjuster : MonoBehaviour
{
    private RectTransform canvasTrans;
    private RectTransform upButtonTrans;
    private RectTransform downButtonTrans;
    private RectTransform rightButtonTrans;
    private RectTransform leftButtonTrans;

    public float arrow_w;
    public float arrow_h;

    // Start is called before the first frame update
    void Start()
    {
        canvasTrans = GetComponent<RectTransform>();

        upButtonTrans = GameObject.Find("UpButton").GetComponent<RectTransform>();
        downButtonTrans = GameObject.Find("DownButton").GetComponent<RectTransform>();
        rightButtonTrans = GameObject.Find("RightButton").GetComponent<RectTransform>();
        leftButtonTrans = GameObject.Find("LeftButton").GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        var width = canvasTrans.rect.width;
        var height = canvasTrans.rect.height;

        upButtonTrans.anchoredPosition = new Vector3(0.0f, height / 2.0f * 0.95f, 0.0f);
        upButtonTrans.sizeDelta = new Vector2(width * arrow_w, height * arrow_h);

        downButtonTrans.anchoredPosition = new Vector3(0.0f, -height / 2.0f * 0.95f, 0.0f);
        downButtonTrans.sizeDelta = new Vector2(width * arrow_w, height * arrow_h);

        rightButtonTrans.anchoredPosition = new Vector3(width / 2.0f * 0.95f, 0.0f, 0.0f);
        rightButtonTrans.sizeDelta = new Vector2(width * arrow_w, height * arrow_h);

        leftButtonTrans.anchoredPosition = new Vector3(- width / 2.0f * 0.95f, 0.0f, 0.0f);
        leftButtonTrans.sizeDelta = new Vector2(width * arrow_w, height * arrow_h);
    }
}
