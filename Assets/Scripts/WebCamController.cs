﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCamController : MonoBehaviour
{
    // Public variables
    //public int deviceID;
    public GameObject selectObject;
    public GameObject playObject;

    // Private variables
    private Projector projector;
    private Dropdown select;
    private Animator animator;
    
    //private WebCamDevice[] devices;
    private WebCamTexture webCamTexture = null;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        projector = GetComponent<Projector>();
        projector.material.mainTexture = Resources.Load("now_loading", typeof(Texture2D)) as Texture2D;

        select = selectObject.GetComponent<Dropdown>();

        /*
        if(webCamTexture != null)
        {
            GetComponent<Projector>().material.mainTexture = webCamTexture;

            if (!webCamTexture.isPlaying)
            {
                webCamTexture.Play();
            }
        }
        else
        {            
            projector.material.mainTexture = Resources.Load("now_loading", typeof(Texture2D)) as Texture2D;
        }
        */
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CameraCtrl()
    {
        try
        {
            // 選択したデバイスを取得する
            var index = select.value;
            var str = select.options[index].text.Split(':');
            int dev = Int32.Parse(str[0]) - 1;

            bool status = playObject.GetComponent<PlayCtrl>().PlayStatus;

            if (status)
            {
                // 初期動作時
                if (webCamTexture == null)
                {
                    WebCamDevice[] devices = WebCamTexture.devices;
                    webCamTexture = new WebCamTexture(devices[dev].name, 1920, 1080, 30);
                }
                else
                {
                    webCamTexture.Stop();                    
                }

                GetComponent<Projector>().material.mainTexture = webCamTexture;
                webCamTexture.Play();
            }
            else
            {
                if (webCamTexture != null)
                {
                    webCamTexture.Stop();
                }

                projector.material.mainTexture = Resources.Load("now_loading", typeof(Texture2D)) as Texture2D;
            }
        }
        catch(FormatException e)
        {
            Debug.Log(e.Message);
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    public void Reset()
    {
        animator.SetBool("EXIT", true);
    }

    public void Init()
    {
        animator.SetBool("EXIT", false);
    }
}
